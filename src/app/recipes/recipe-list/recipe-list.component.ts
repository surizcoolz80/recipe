import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.modal';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [
    new Recipe('A Test Recipe',
    'This is simply test',
    'https://www.freshcityfarms.com/imagefly/w550-h367-c/resources/media/images/recipes/url_486.jpg'),
    new Recipe('A Test Recipe',
    'This is simply test',
    'https://www.freshcityfarms.com/imagefly/w550-h367-c/resources/media/images/recipes/url_486.jpg'),
    
    new Recipe('A Test Recipe','This is simply test','https://www.freshcityfarms.com/imagefly/w550-h367-c/resources/media/images/recipes/url_486.jpg') 
  ];

  constructor() { }

  ngOnInit() {
  }

}
